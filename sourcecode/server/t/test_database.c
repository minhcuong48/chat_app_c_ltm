#include <stdio.h>
#include <string.h>

#include <database.h>

static int on_get_row(void* NotUsed, int num_cols, char** row, char** cols) {
	printf("%s-%s-%s\n", row[0], row[1], row[2]);
	return 0;
}

int main(int argc, char const *argv[])
{
	sqlite3** db = open_database();
	if(db != NULL) {
		printf("db != NULL\n");
	} else {
		printf("db == NULL\n");
	}

	

	// get user
	char sql_get_user[] = "SELECT * FROM users WHERE user_id = 1;";
	select_query(sql_get_user, on_get_row, 0);

	close_database(db);
	return 0;
}

void insert_a_user() {
	// insert a user
	char sql[] = "INSERT INTO " \
					"users(username, password) VALUES " \
					"('gminhcuong', '123456')";
	if(insert_query(sql)) {
		printf("TEST_DATABASE: Success\n->\"%s\"\n", sql);
	} else {
		printf("TEST_DATABASE: Failed\n->\"%s\"\n", sql);
	}
}
