#include <stdio.h>
#include <linked_list.h>

int main(int argc, char const *argv[])
{
	List list = create_list();
	Node* node = create_node("Hello");
	add_node(&list, node);
	node = create_node("World");
	add_node(&list, node);
	node = create_node("I");
	add_node(&list, node);
	node = create_node("Love");
	add_node(&list, node);
	node = create_node("You");
	add_node(&list, node);
	node = create_node("So");
	add_node(&list, node);
	node = create_node("Much");
	add_node(&list, node);
	printf("Count = %d\n", count_nodes(list));
	print_list(list);
	return 0;
}