#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

int sign_up(char* username, char* password);
int login(char* username, char* password, int client_socket_id);
int add_friend(int client, char* receiver);
int accept_invitation(int client, char* requester);
int decline_invitation(int client, char* requester);
int send_text(int client, char* receiver, char* text_mess);
int delete_friend(int client, char* username, char* friend_username);

#endif