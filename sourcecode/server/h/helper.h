#ifndef _HELPER_H_
#define _HELPER_H_

#include <netinet/in.h>
#include <linked_list.h>
#include <database.h>
#include <constant.h>

typedef struct {
	int client;
	struct sockaddr_in client_address;
} ClientInfo;

typedef struct {
	char username[MAX_STRING_LENGTH_TOKEN];
	int client;
	int status;
	int of_user;
	char of_username[MAX_STRING_LENGTH_TOKEN];
} Friend;

typedef struct {
	char username[MAX_STRING_LENGTH_TOKEN];
	int to_user;
} Invitation;

/**
 * Followed functions are used for pass to function to handle database
 */

int on_get_user(void* result, int num_cols, char** row, char** cols);
int on_get_client_username(void* result, int num_cols, char** row, char** cols);
int on_get_client_socket(void* result, int num_cols, char** row, char** cols);
int on_count_rows(void* result, int num_cols, char** row, char** cols);
int on_find_user_id(void* result, int num_cols, char** row, char** cols);
int on_get_friend(void* result, int num_cols, char** row, char** cols);
int on_get_invitation(void* result, int num_cols, char** row, char** cols);
int on_get_friend_to_notify_online(void* result, int num_cols, char** row, char** cols);
int on_get_friend_to_notify_offline(void* result, int num_cols, char** row, char** cols);
/****************************************************************************************************/

void append_int(char* string, int integer);
char* current_date_time();
void log_info(char* string);
int get_user_id(char* username);
int send_message(int client, char* message);
int isEndWithSeparator(const char* message);
char* get_header(char* message);
int count_message_tokens(char* message);
void extract_user_info(char* message, char* username, char* password);
List extract_message_to_list(char* message);
char* sub_string(char* source, int from, int to);
int reset_offline();
int set_offline(int client);
int save_client_id(char* username, int client);
int make_friend(char* sender, char* receiver);
int delete_friend_request(char* sender, char* receiver);
void notify_friend_request_if_receiver_online(char* sender, char* receiver);
int is_friend(char* user1, char* user2);
char* get_client_username(int client);
int get_client_socket(char* username);
int is_exists_user(char* username);
int is_exists_friend_request(char* sender, char* receiver);
void send_friend_list_to(char* username);
void send_friend_info_to(char* username, char* friend);
void send_invitations_to(char* username);
void notify_online_to_friend_of_user(char* username);
void notify_offline_to_friend_of_user(char* username);
int remove_friend_in_database(char* username, char* friend_username);
/****************************************************************************************************/

/**
 * Generate Protocol and Send Methods
 */

char* get_code_101(char* success_message);
char* get_code_102(char* error_message);
char* get_code_201();
void send_code_201(int client);
char* get_code_204(char* me);
char* get_code_205(char* error_message);
void send_code_205(int client);
char* get_code_302(char* username);
char* get_code_603(char* requester, char* error_message);
void send_code_603(char* requester, char* receiver);
char* get_code_601(char* requester, char* message);
void send_code_601(char* requester, char* receiver);
char* get_code_602(char* message);
void send_code_602(char* requester, char* receiver);
char* get_code_501(char* requester, char* success_message);
void send_code_501(char* requester, char* receiver);
char* get_code_502(char* receiver);
void send_code_502(char* requester, char* receiver);
char* get_code_503(char* requester, char* error_message);
void send_code_503(char* requester, char* receiver, char* error_message);
char* get_code_402(char* sender);
char* get_code_401(char* success_message);
char* get_code_403(char* error_message);
void send_code_success_401(int client, char* message);
void send_code_error_403(int client, char* message);
char* get_code_202(char* friend_username, int status);
void send_code_202(int client, char* friend_username, int status);
char* get_code_801(char* success_message);
void send_code_801(int sender_socket, char* success_message);
char* get_code_802(char* sender, char* message);
void send_code_802(int sender_socket, int receiver_socket, char* text_message);
char* get_code_803(char* receiver, char* error_message);
void send_code_803(int sender_socket, char* receiver, char* error_message);
char* get_code_701(char* friend_username, char* message);
void send_code_701(int client, char* friend_username, char* message);
char* get_code_702(char* username);
void send_code_702(char* username, char* friend_username);
char* get_code_703(char* friend_username, char* message);
void send_code_703(int client, char* friend_username, char* message);
char* get_code_203(char* sender);
void send_code_203(int client, char* sender);

#endif