#ifndef _HANDLE_REQUEST_H_
#define _HANDLE_REQUEST_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>

void on_client_connected(int client, struct sockaddr_in client_address);
void on_client_send_message(int client, char* message, int message_length, struct sockaddr_in client_address);
void on_client_disconnected(int client, struct sockaddr_in client_address);

void handle_sign_up(int client, char* message, struct sockaddr_in client_address);
void handle_login(int client, char* message, struct sockaddr_in client_address);
void handle_add_friend(int client, char* message, struct sockaddr_in client_address);
void handle_accept_friend(int client, char* message, struct sockaddr_in client_address);
void handle_decline_friend(int client, char* message, struct sockaddr_in client_address);
void handle_send_message(int client, char* message, struct sockaddr_in client_address);
void handle_delete_friend(int client, char* message, struct sockaddr_in client_address);

#endif