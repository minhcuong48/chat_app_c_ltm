/*
* @Author: GiapMinhCuong-20140539
* @Date:   2017-11-01 01:35:04
* @Last Modified by:   GiapMinhCuong-20140539
* @Last Modified time: 2017-11-02 01:49:19
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <handle_request.h>
#include <constant.h>
#include <helper.h>

/*************************** LIFE CYCLE ************************************************************/
// ham nay xu ly khi bat dau tao ra 1 tien trinh con phuc vu cho 1 client
void handle_client_begin(int client, struct sockaddr_in client_address) {
	//printf("INFO:%s:%d: handle_client_begin()\n", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port));
	on_client_connected(client, client_address);
}

// ham nay se duoc thuc thi tren 1 tien trinh con ung voi moi client connection.
void handle_client_message(int client, char* message, int message_length, struct sockaddr_in client_address) {
	//printf("INFO: Got %d byte(s) from Client: message=\"%s\"(strlen=%d)\n", (int)message_length, message, (int)strlen(message));
	char _info[MAX_STRING_LENGTH_PROTOCOL];
	char* _username = get_client_username(client);
	if(strcmp(_username, "") == 0) {
		sprintf(_info, "Got \"%s\" from client socket %d", message, client);
	} else {
		sprintf(_info, "Got \"%s\" from user \"%s\" at client socket %d", message, _username, client);
	}
	log_info(_info);

	on_client_send_message(client, message, message_length, client_address);
}

// ham nay duoc goi moi khi client ngat ket noi
void handle_client_closed(int client, struct sockaddr_in client_address) {
	//printf("INFO: Client %s:%d closed\n", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port));
	on_client_disconnected(client, client_address);
}
/***************************************************************************************************/

void* handle_new_client(void* args) {
	ClientInfo* info = (ClientInfo*)args;
	int client = info->client;
	struct sockaddr_in client_address = info->client_address;
	handle_client_begin(client, client_address);

	// 5 Receiver data
	ssize_t message_length;
	char message[MESSAGE_LENGTH];
	bzero(message, MESSAGE_LENGTH);
	while ((message_length = recv(client, &message, MESSAGE_LENGTH, 0)) > 0) {
		char* client_ip = inet_ntoa(client_address.sin_addr);
		// printf("Got %d byte(s) from Client %s: %s(%d)\n", (int)message_length, client_ip, message, (int)strlen(message));
		char* copy_message = (char*)malloc(MESSAGE_LENGTH);
		strcpy(copy_message, message);
		handle_client_message(client, copy_message, message_length, (struct sockaddr_in)client_address);
		bzero(message, MESSAGE_LENGTH);
	}

	// 5a Check recv error
	if (message_length < 0) {
		printf("ERROR: recv()\n");
		return (void*)5;
	}

	// 5b Check client close
	if (message_length == 0) {
		// printf("INFO: Client closed!\n");
		//printf("INFO: Client %s:%d closed\n", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port));
		handle_client_closed(client, (struct sockaddr_in)client_address);
		return (void*)0;
	}

	return (void*)0;
}

// MAIN
int main(int argc, char* argv[]) {
	// reset every user to offline
	reset_offline();

	// 1. Tao socket
	int server = socket(AF_INET, SOCK_STREAM, 0);
	if (server < 0) {
		// printf("ERROR: socket()!\n");
		log_info("Opening server socket is failed");
		return -1;
	}

	// 2. Bind socket
	// 2.1 Tao sockaddr_in
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	server_address.sin_port = htons(PORT);

	// 2.2 Bind
	int bind_result = bind(server, (struct sockaddr*)&server_address, sizeof(server_address));
	if (bind_result < 0) {
		// printf("ERROR: bind()\n");
		log_info("Binding server socket is failed");
		return -1;
	}

	// 3 Listen connection
	int listen_result = listen(server, MAX_CONNECTION);
	if (listen_result < 0) {
		// printf("ERROR: listen()\n");
		log_info("Listening server socket is failed");
		return -1;
	}

	//printf("INFO: Waiting for connection at port %d ...\n", ntohs(server_address.sin_port));
	char _info[MAX_STRING_LENGTH_PROTOCOL];
	strcpy(_info, "Waitting for connections at port ");
	append_int(_info, ntohs(server_address.sin_port));
	log_info(_info);

	while(1) {
		// 4 Accept connection
		// 4.1 Tao ra cac bien luu tru gia tri tra ve
		struct sockaddr_in client_address;
		socklen_t client_address_length;
		bzero(&client_address, sizeof(client_address));

		int client = accept(server, (struct sockaddr*)&client_address, &client_address_length);
		
		// if accept faild then continue waitting other connection 
		if (client < 0) {
			//printf("ERROR: accept()\n");
			log_info("Accepting new connection is failed");
			continue;
		}

		// if accept success
		if (client >= 0) {
			//printf("INFO: Connected to Client %s:%d; client_id = %d\n", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port), client);
			char _info[MAX_STRING_LENGTH_PROTOCOL] = "";
			sprintf(_info, "Client at %s:%d connected at socket %d", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port), client);
			log_info(_info);

			ClientInfo* info = (ClientInfo*)malloc(sizeof(ClientInfo));
			info->client = client;
			info->client_address = (struct sockaddr_in)client_address;
			pthread_t child_thread;
			pthread_create(&child_thread, NULL, &handle_new_client, info);
		}
	
	}
	
	// 6 Close socket
	close(server);

	return 0;
}