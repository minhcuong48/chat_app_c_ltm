#include <functions.h>
#include <constant.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <database.h>
#include <helper.h>

int sign_up(char* username, char* password) {
	// create sql query
	char* sql = (char*)malloc(MAX_STRING_LENGTH_SQL);
	strcpy(sql, "");
	strcat(sql, "INSERT INTO users(username, password) VALUES ('");
	strcat(sql, username);
	strcat(sql, "', '");
	strcat(sql, password);
	strcat(sql, "');");

	return insert_query(sql);
}

// helper
void print_success_login(char* username, char* password) {
    char _info[MAX_STRING_LENGTH_PROTOCOL];
    sprintf(_info, "User \"%s\" has logged in successfully at socket %d", username, get_client_socket(username));
    log_info(_info);
}

void print_error_login(char* username, char* password, int client) {
    char _info[MAX_STRING_LENGTH_PROTOCOL];
    sprintf(_info, "User \"%s\" failed to log in at socket %d", username, client);
    log_info(_info);
}

int login(char* username, char* password, int client_socket_id) {
	// printf("INFO: login() is started\n");

	char sql[MAX_STRING_LENGTH_SQL] = "SELECT * FROM users WHERE username = '";
	strcat(sql, username);
	strcat(sql, "';");
	char stored_password[MAX_STRING_LENGTH_TOKEN] = "";
	select_query(sql, on_get_user, &stored_password);

	// if username is not exist
	if(strlen(stored_password) == 0) {
		//printf("Login faild: Username is not exist\n");
        print_error_login(username, password, client_socket_id);
        send_code_205(client_socket_id);
		return 0;
	}

	// if password is not matched
	if(strcmp(stored_password, password) != 0) {
		// printf("Login faild: Password is not matched\n");
        print_error_login(username, password, client_socket_id);
        send_code_205(client_socket_id);
		return 0;
	}

	// check if logged
	int client_socket = get_client_socket(username);
	if(client_socket >= 0) {
		// printf("Login faild: You have to logout this account in other machine first!\n");
        print_error_login(username, password, client_socket_id);
        send_code_205(client_socket_id);
		return 0;
	}

	if(!save_client_id(username, client_socket_id)) {
		// printf("Login faild: Cant save client socket id\n");
        print_error_login(username, password, client_socket_id);
        send_code_205(client_socket_id);
		return 0;
	}

    // log in success if come here

    print_success_login(username, password);
    send_code_201(client_socket_id);
    send_friend_list_to(username);
    send_invitations_to(username);
    notify_online_to_friend_of_user(username);

	return 1;
}

int add_friend(int client, char* receiver) {
    char* sender = get_client_username(client);

    // printf("sender = %s, receiver = %s\n", sender, receiver);
    // printf("is_exists_user = %d\n", is_exists_user(sender));
    // printf("is_exists_friend_request = %d\n", is_exists_friend_request(sender, receiver));

    if(strcmp(sender, "") == 0 || is_exists_user(sender) == 0) {
        // printf("Username %s is not exists\n", sender);
        send_code_error_403(client, "You must log in first!");
        return 0;
    }

    if(strcmp(receiver, "") == 0 || is_exists_user(receiver) == 0) {
        // printf("Username %s is not exists\n", receiver);
        char message[MAX_STRING_LENGTH_PROTOCOL] = "Username '";
        strcat(message, receiver);
        strcat(message, "' is not exists!");
        send_code_error_403(client, message);
        return 0;
    }

    if(is_exists_friend_request(sender, receiver) != 0) {
        // printf("Friend request from %s to %s is exists\n", sender, receiver);
        send_code_error_403(client, "You have sent this friend request before!");
        return 0;
    }

    if(is_exists_friend_request(receiver, sender) != 0) {
        // printf("Friend request from %s to %s is exists\n", sender, receiver);
        send_code_error_403(client, "You already got friend request from this person!");
        return 0;
    }

    if(is_friend(sender, receiver) || is_friend(receiver, sender)) {
        // printf("Both of them are already friends!\n");
        send_code_error_403(client, "Both of you are already friends!");
        return 0;
    }

    char sender_id_string[MAX_STRING_LENGTH_TOKEN] = "";
    char receiver_id_string[MAX_STRING_LENGTH_TOKEN] = "";

    sprintf(sender_id_string, "%d", get_user_id(sender));
    sprintf(receiver_id_string, "%d", get_user_id(receiver));

    char sql[MAX_STRING_LENGTH_SQL] = "insert into friend_requests(sender, receiver) values ('";
    strcat(sql, sender_id_string);
    strcat(sql, "', '");
    strcat(sql, receiver_id_string);
    strcat(sql, "')");

    int isOk = insert_query(sql);

    if(!isOk) {
        printf("Saving friend request from %s to %s to database fail!\n", sender, receiver);
        return 0;
    }
    send_code_success_401(client, "Your friend request is sent!");
    notify_friend_request_if_receiver_online(sender, receiver);

    return 1;
}

int accept_invitation(int client, char* requester) {
    // printf("Accept invitation from client %d for requester %s!\n", client, requester);

    char* receiver;
    receiver = get_client_username(client);

    if(strcmp(requester, "") == 0 || is_exists_user(requester) == 0) {
        // printf("Requester %s is not exists\n", requester);
        send_code_503(requester, receiver, "Requester username is not valid");
        return 0;
    }

    if(strcmp(receiver, "") == 0) {
        // printf("Reiceiver %s is not exists\n", receiver);
        send_code_503(requester, receiver, "Your username is not valid!");
        return 0;
    }

    if(is_exists_friend_request(requester, receiver) == 0) {
        // printf("Friend request is not exists!\n");
        send_code_503(requester, receiver, "Friend request is not exists!");
        return 0;
    }

    if(is_friend(receiver, requester) || is_friend(requester, receiver)) {
        // printf("They are friend of each other!\n");
        send_code_503(requester, receiver, "Both of you are already friend!");
        return 0;
    }

    if(delete_friend_request(requester, receiver)) {
        if(make_friend(requester, receiver)
        && make_friend(receiver, requester)) {
            send_code_501(requester, receiver);
            send_code_502(requester, receiver);
            // send code 202 to append new friend to both of receiver and requester's list friends
            send_friend_info_to(receiver, requester);
            // send_friend_info_to(requester, receiver);
            // send to the person who is accepted invitation - sender invitation
        }
    }

    return 1;
}

int decline_invitation(int client, char* requester) {
    // printf("Decline invitation from client %d for requester %s!\n", client, requester);

    char* receiver;
    receiver = get_client_username(client);

    if(strcmp(requester, "") == 0 || is_exists_user(requester) == 0) {
        // printf("Requester %s is not exists\n", requester);
        send_code_603(requester, receiver);
        return 0;
    }

    if(strcmp(receiver, "") == 0) {
        // printf("Reiceiver %s is not exists\n", receiver);
        send_code_603(requester, receiver);
        return 0;
    }

    if(is_exists_friend_request(requester, receiver) == 0) {
        // printf("Friend request is not exists!\n");
        send_code_603(requester, receiver);
        return 0;
    }

    if(is_friend(receiver, requester) || is_friend(requester, receiver)) {
        // printf("They are friend of each other!\n");
        send_code_603(requester, receiver);
        return 0;
    }

    // if decline success
    if(delete_friend_request(requester, receiver)) {
        send_code_601(requester, receiver);
        send_code_602(requester, receiver);
    }

    return 1;
}

int send_text(int client, char* receiver, char* text_mess) {
    char* sender_username = get_client_username(client);
    if(sender_username == NULL || strcmp(sender_username, "") == 0) {
        send_code_803(client, receiver, "You have to login first");
        return 0;
    }

    if(!is_friend(sender_username, receiver)) {
        send_code_803(client, receiver, "I am not your friend!");
        return 0;
    }

    int receiver_client = get_client_socket(receiver);
    if (receiver_client == -1) {
        send_code_803(client, receiver, "I am not online!");
        return 0;
    }

    send_code_802(client, receiver_client, text_mess);
    send_code_801(client, "Your message is sent!");
    return 1;
}

int delete_friend(int client, char* username, char* friend_username) {
    if(remove_friend_in_database(username, friend_username)
        && remove_friend_in_database(friend_username, username)) {
        // send code 701 to client
        char* _message = (char*) malloc(MAX_STRING_LENGTH_PROTOCOL);
        strcpy(_message, "Deleted ");
        strcat(_message, friend_username);
        strcat(_message, "!");
        send_code_701(client, friend_username, _message);
        // send code 702 to deleted friend client
        send_code_702(username, friend_username);
        return 1;
    } else {
        // send code 703 to client
        char* _message = (char*) malloc(MAX_STRING_LENGTH_PROTOCOL);
        strcpy(_message, "Deleting '");
        strcat(_message, friend_username);
        strcat(_message, "' is failed!");
        send_code_703(client, friend_username, _message);
    }
    return 0;
}