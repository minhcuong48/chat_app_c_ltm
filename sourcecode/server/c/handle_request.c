#include <handle_request.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <constant.h>
#include <helper.h>
#include <functions.h>

void on_client_connected(int client, struct sockaddr_in client_address) {
	//printf("HANDLE_REQUEST_CONNECTED\n");
	// char _info[MAX_STRING_LENGTH_PROTOCOL];
	// sprintf(_info, "Client %d has connected", client);
	// log_info(_info);
}

void on_client_send_message(int client, char* message, int message_length, struct sockaddr_in client_address) {
	//printf("HANDLE_REQUEST_SEND_MESSAGE\n");
	char* header = get_header(message);

	if(header == NULL) {
		// printf("Header == NULL\n");
		return;
	}

	// if(strcmp(header, "send") == 0 && !isEndWithSeparator(message)) {

	// 	List l = extract_message_to_list(message);
	// 	char* client_string = get_node_string(2, l);
	// 	char* client_message = get_node_string(3, l);
	// 	int client = atoi(client_string);
	// 	// printf("client = %s, message = %s\n", client_string, client_message);
	// 	send_message(client, client_message);
	// 	return;
	// }

	if (strcmp(header, SIGN_UP) == 0 && count_message_tokens(message) == SIGN_UP_NUM_TOKENS) {
		// printf("INFO: Got a request for signing up\n");
		handle_sign_up(client, message, client_address);
	}
	else if(strcmp(header, LOG_IN) == 0 && count_message_tokens(message) == LOG_IN_NUM_TOKENS) {
		// printf("INFO: Got a request for logging in\n");
		handle_login(client, message, client_address);
	}
	else if(strcmp(header, FREQUEST) == 0 && count_message_tokens(message) == FREQUEST_NUM_TOKENS) {
		// printf("INFO: Got a request for add friend\n");
		handle_add_friend(client, message, client_address);
	}
	else if(strcmp(header, ACCR) == 0 && count_message_tokens(message) == ACCR_NUM_TOKENS) {
		handle_accept_friend(client, message, client_address);
	}
	else if(strcmp(header, DECR) == 0 && count_message_tokens(message) == DECR_NUM_TOKENS) {
		handle_decline_friend(client, message, client_address);
	}
	else if(strcmp(header, MESS) == 0 && count_message_tokens(message) == MESS_NUM_TOKENS) {
		handle_send_message(client, message, client_address);
	}
	else if(strcmp(header, DELF) == 0 && count_message_tokens(message) == DELF_NUM_TOKENS) {
		handle_delete_friend(client, message, client_address);
	}
}

void on_client_disconnected(int client, struct sockaddr_in client_address) {
	// printf("HANDLE_REQUEST_DISCONNECTED\n");
	// send message offline before set offline for user
	char* username = get_client_username(client);
	notify_offline_to_friend_of_user(username);
	// do this at last
	set_offline(client);

	char _info[MAX_STRING_LENGTH_PROTOCOL];
	sprintf(_info, "Client %d disconnected", client);
	log_info(_info);
}

// extract information from sign up message
void handle_sign_up(int client, char* message, struct sockaddr_in client_address) {
	// printf("INFO: Started handle signing up for client %d\n", client);

	char* username = (char*)malloc(MAX_STRING_LENGTH_TOKEN);
	char* password = (char*)malloc(MAX_STRING_LENGTH_TOKEN);

	char _message[strlen(message) + 1];
	strcpy(_message, message);
	extract_user_info(_message, username, password);

	int isOk = sign_up(username, password);
	if(!isOk) {
		char _info[MAX_STRING_LENGTH_PROTOCOL];
		sprintf(_info, "Client %d failed to sign up with username \"%s\" and password \"%s\"", client, username, password);
		log_info(_info);

		// respond code to client
		char* code_102 = get_code_102("Signing up is failed");
		send_message(client, code_102);
		free(code_102);
	} else {
		char _info[MAX_STRING_LENGTH_PROTOCOL];
		sprintf(_info, "Client %d has signed up successfully with username \"%s\" and password \"%s\"", client, username, password);
		log_info(_info);

		// respond code to client
		char* code_101 = get_code_101("Signing up is successful");
		send_message(client, code_101);
		free(code_101);
	}

	free(username);
	free(password);
}

// extract information from login message
void handle_login(int client, char* message, struct sockaddr_in client_address) {
	// printf("INFO: Started handle logging in for client %d\n", client);

	char* username = (char*)malloc(MAX_STRING_LENGTH_TOKEN);
	char* password = (char*)malloc(MAX_STRING_LENGTH_TOKEN);

	extract_user_info(message, username, password);
	login(username, password, client);

	free(username);
	free(password);
}

// extract information from friend request message
void handle_add_friend(int client, char* message, struct sockaddr_in client_address) {
	// printf("INFO: Started handle add friend for client %d\n", client);
	List tokens = extract_message_to_list(message);
	char* receiver = get_node_string(2, tokens);
	add_friend(client, receiver);

	free(receiver);
}

// extract information from accept request message
void handle_accept_friend(int client, char* message, struct sockaddr_in client_address) {
	// printf("INFO: Started handle accept request for client %d\n", client);
	List tokens = extract_message_to_list(message);
	char* requester = get_node_string(2, tokens);
	accept_invitation(client, requester);

	free(requester);
}

void handle_decline_friend(int client, char* message, struct sockaddr_in client_address) {
	// printf("INFO: Started handle decline request for client %d\n", client);
	List tokens = extract_message_to_list(message);
	char* requester = get_node_string(2, tokens);
	decline_invitation(client, requester);

	free(requester);
}

void handle_send_message(int client, char* message, struct sockaddr_in client_address) {
	// printf("INFO: Started handle send message text from client %d\n", client);
	List tokens = extract_message_to_list(message);
	char* receiver = get_node_string(2, tokens);
	char* text_message = get_node_string(3, tokens);
	send_text(client, receiver, text_message);

	free(receiver);
	free(text_message);
}

void handle_delete_friend(int client, char* message, struct sockaddr_in client_address) {
	// printf("INFO: Started handle send message text from client %d\n", client);
	char* username = get_client_username(client);
	List tokens = extract_message_to_list(message);
	char* friend_username = get_node_string(2, tokens);
	
	delete_friend(client, username, friend_username);

	free(username);
	free(friend_username);
}