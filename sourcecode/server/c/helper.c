#include <helper.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <constant.h>
#include <linked_list.h>
#include <database.h>

/**
 * Followed functions are used for pass to function to handle database
 */

int on_get_user(void* result, int num_cols, char** row, char** cols) {
	char* password = (char*)result;
	if(strcmp(cols[2], "password") == 0) {
		strcpy(password, row[2]);
	} else {
		strcpy(password, "");
	}
	return 0;
}

int on_get_client_username(void* result, int num_cols, char** row, char** cols) {
    char* username = (char*)result;
	if(strcmp(cols[0], "username") == 0) {
		strcpy(username, row[0]);
	} else {
		strcpy(username, "");
	}
	return 0;
}

int on_get_client_socket(void* result, int num_cols, char** row, char** cols) {
	int* client = (int*)result;
	if(strcmp(cols[0], "client") == 0) {
		*client = atoi(row[0]);
	} else {
		*client = -1;
	}
	return 0;
}

int on_count_rows(void* result, int num_cols, char** row, char** cols) {
	int* count = (int*)result;
	if(strcmp(cols[0], "count") == 0) {
		*count = atoi(row[0]);
	} else {
		*count = 0;
	}
	return 0;
}

int on_find_user_id(void* result, int num_cols, char** row, char** cols) {
	int* user_id = (int*)result;
	if(strcmp(cols[0], "user_id") == 0) {
		*user_id = atoi(row[0]);
	} else {
		*user_id = -1;
	}
	return 0;
}

int get_user_id(char* username) {
    char sql[MAX_STRING_LENGTH_SQL] = "select user_id from users where username = '";
    strcat(sql, username);
    strcat(sql, "'");
    int user_id = -1;
    select_query(sql, on_find_user_id, &user_id);
    return user_id;
}

/**********************************************************************************************************/

int send_message(int client, char* message) {
    if(client == -1) return 0;
	char tmp[MAX_STRING_LENGTH_PROTOCOL] = "";
	strcpy(tmp, message);
	ssize_t sent_length;
	sent_length = send(client, message, strlen(message), 0);
	if((int)sent_length > 0) {
		// printf("INFO: Sent %d byte(s) to client %d: \"%s, strlen = %d\"\n", (int)(sent_length), client, message, (int)strlen(message));
        char _info[MAX_STRING_LENGTH_PROTOCOL];
        char* username = get_client_username(client);
        if (strcmp(username, "") == 0) {
            sprintf(_info, "Sent \"%s\" to client socket %d", message, client);
        } else {
            sprintf(_info, "Sent \"%s\" to user \"%s\" at socket %d", message, username, client);
        }
        
        log_info(_info);
    }
	return (int)sent_length;
}

int isEndWithSeparator(const char* message) {
	if(message[strlen(message) - 1] == SEPARATOR[0]) {
		return 1;
	}
	return 0;
}

char* get_header(char* message) {
	// printf("Start checking header\n");
	int len = strlen(message);
	char tmp[len+1];
	strcpy(tmp, message);

	char* header = strtok(tmp, SEPARATOR);
	// printf("HEADER = \"%s\"\n", header);
	// printf("Finish checking header\n");
	return header;
}

int count_message_tokens(char* message) {
	// printf("Start counting message tokens\n");
	List list = extract_message_to_list(message);
	// printf("Finish counting message tokens\n");
	return count_nodes(list);
}

void extract_user_info(char* message, char* username, char* password) {
	List list = extract_message_to_list(message);
	char* _username = get_node_string(2, list);
	char* _password = get_node_string(3, list);

	strcpy(username, _username);
	strcpy(password, _password);
	
	free(_username);
	free(_password);
}

List extract_message_to_list(char* message) {
	List list = create_list();
	int len = strlen(message);
	int from = 0, to = 0, i = 0;
	char separator[2] = SEPARATOR;
	for(i=0; i<len; i++) {
		if(i == 0 || message[i-1] == separator[0]) {
			from = i;
		}
		if (i == len-1 || message[i+1] == separator[0]) {
			to = i;
			char* token = sub_string(message, from, to);
			add_node(&list, create_node(token));
		}
	}
	return list;
}

char* sub_string(char* source, int from, int to) {
	int source_len = strlen(source);
	int i = 0;
	int dest_len = to - from + 1;
	char* dest = malloc(dest_len + 1);
	for (i = 0; i < dest_len; i++) {
		dest[i] = source[i + from];
	}
	dest[i] = 0;
	return dest;
}

char* get_code_101(char* success_message) {
	char* code = malloc(MAX_STRING_LENGTH_PROTOCOL);
	code[0] = 0;
	strcat(code, "101|");
	strcat(code, success_message);
	return code;
}

char* get_code_102(char* error_message) {
	char* code = malloc(MAX_STRING_LENGTH_PROTOCOL);
	code[0] = 0;
	strcat(code, "102|");
	strcat(code, error_message);
	return code;
}

char* get_code_201() {
	char* code = malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "201|Logging in is successful");
	return code;
}

char* get_code_205(char* error_message) {
	char* code = malloc(MAX_STRING_LENGTH_PROTOCOL);
	code[0] = 0;
	strcat(code, "205|");
	strcat(code, error_message);
	return code;
}

int reset_offline() {
	char sql[MAX_STRING_LENGTH_SQL] = "update users set client = -1";
	return insert_query(sql);
}

int set_offline(int client) {
	char client_string[MAX_STRING_LENGTH_TOKEN] = "";
	sprintf(client_string, "%d", client);

	char sql[MAX_STRING_LENGTH_SQL] = "update users set client = -1 where client = ";
	strcat(sql, client_string);
    char* me = get_client_username(client);
    notify_offline_to_friend_of_user(me);
    
    if(strcmp(me, "") != 0) {
        char _info[MAX_STRING_LENGTH_PROTOCOL];
        sprintf(_info, "User \"%s\" go offline at socket %d", me, client);
        log_info(_info);
    }

    free(me);
	return insert_query(sql);
}



int save_client_id(char* username, int client) {
	char client_string[MAX_STRING_LENGTH_TOKEN] = "";
	sprintf(client_string, "%d", client);

	char sql[MAX_STRING_LENGTH_SQL] = "update users set client = ";
	strcat(sql, client_string);
	strcat(sql, " where username = '");
	strcat(sql, username);
	strcat(sql, "'");

	return insert_query(sql);
}

char* get_code_603(char* requester, char* error_message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "603|");
    strcat(code, error_message);
    return code;
}

void send_code_603(char* requester, char* receiver) {
    int client = get_client_socket(receiver);
    char* message = get_code_603(requester, "Your declining is failed!");
    send_message(client, message);
    free(message);
}

char* get_code_601(char* requester, char* message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "601|");
    strcat(code, requester);
    strcat(code, "|");
    strcat(code, message);
    return code;
}

void send_code_601(char* requester, char* receiver) {
    int client = get_client_socket(receiver);
    char* _message = (char*) malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(_message, "You declined the invitation from ");
    strcat(_message, requester);
    strcat(_message, "!");
    char* message = get_code_601(requester, _message);
    send_message(client, message);
    free(message);
}

char* get_code_602(char* message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "602|");
    strcat(code, message);
    return code;
}

void send_code_602(char* requester, char* receiver) {
    int client = get_client_socket(requester);
    char _message[MAX_STRING_LENGTH_PROTOCOL] = "You are declined friend request by ";
    strcat(_message, receiver);
    char* message = get_code_602(_message);
    send_message(client, message);
    free(message);
}

char* get_code_501(char* requester, char* success_message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "501|");
    strcat(code, requester);
    strcat(code, "|");
    strcat(code, success_message);
    return code;
}

void send_code_501(char* requester, char* receiver) {
    int client = get_client_socket(receiver);
    char _message[MAX_STRING_LENGTH_PROTOCOL] = "You accepted request friend from ";
    strcat(_message, requester);
    char* message = get_code_501(requester, _message);
    send_message(client, message);
    free(message);    
}

char* get_code_502(char* receiver) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "502|");
    strcat(code, receiver);
    strcat(code, "|1");
    return code;
}

void send_code_502(char* requester, char* receiver) {
    int client = get_client_socket(requester);
    char* message = get_code_502(receiver);
    send_message(client, message);
    free(message);
}

char* get_code_503(char* requester, char* error_message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "503|");
    strcat(code, requester);
    strcat(code, "|");
    strcat(code, error_message);
    return code;
}

void send_code_503(char* requester, char* receiver, char* error_message) {
    int client = get_client_socket(receiver);
    char* message = get_code_503(requester, error_message);
    send_message(client, message);
    free(message);
}

int make_friend(char* sender, char* receiver) {
    int user_id1 = get_user_id(sender);
    int user_id2 = get_user_id(receiver);

    char user_id1_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id1_string, "%d", user_id1);
    
    char user_id2_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id2_string, "%d", user_id2);

    char sql[MAX_STRING_LENGTH_SQL] = "insert into friends(user, friend) values (";
    strcat(sql, user_id1_string);
    strcat(sql, ", ");
    strcat(sql, user_id2_string);
    strcat(sql, ")");

    return insert_query(sql);
}

int delete_friend_request(char* sender, char* receiver) {
    int user_id1 = get_user_id(sender);
    int user_id2 = get_user_id(receiver);

    char user_id1_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id1_string, "%d", user_id1);
    
    char user_id2_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id2_string, "%d", user_id2);

    char sql[MAX_STRING_LENGTH_SQL] = "delete from friend_requests where sender = ";
    strcat(sql, user_id1_string);
    strcat(sql, " AND receiver = ");
    strcat(sql, user_id2_string);

    return insert_query(sql);
}

void notify_friend_request_if_receiver_online(char* sender, char* receiver) {
    char _info[MAX_STRING_LENGTH_PROTOCOL];
    sprintf(_info, "Notify friend request from \"%s\" to \"%s\"", sender, receiver);
    log_info(_info);

    // printf("Notify to receiver if they online!\n");
    int client = get_client_socket(receiver);
    // printf("receiver freq = %d\n", client);
    if (client >= 0) {
        char* code_402 = get_code_402(sender);
        send_message(client, code_402);
        // printf("run code_402:%s\n", code_402);
        free(code_402);
    }
}

int is_friend(char* user1, char* user2) {
    int count = 0;
    int user_id1 = get_user_id(user1);
    int user_id2 = get_user_id(user2);

    char user_id1_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id1_string, "%d", user_id1);
    
    char user_id2_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id2_string, "%d", user_id2);

    char sql[MAX_STRING_LENGTH_SQL] = "select count(*) as count from friends where user = ";
    strcat(sql, user_id1_string);
    strcat(sql, " AND friend = ");
    strcat(sql, user_id2_string);
    select_query(sql, on_count_rows, &count);
    return count;
}



char* get_client_username(int client) {
    char client_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(client_string, "%d", client);

    char sql[MAX_STRING_LENGTH_SQL] = "select username from users where client = ";
    strcat(sql, client_string);
    char* username = (char*)malloc(MAX_STRING_LENGTH_TOKEN);
    select_query(sql, on_get_client_username, username);
    return username;
}



int get_client_socket(char* username) {
    char sql[MAX_STRING_LENGTH_SQL] = "select client from users where username = '";
    strcat(sql, username);
    strcat(sql, "'");
    int client = -1;
    select_query(sql, on_get_client_socket, &client);
    return client;
}



int is_exists_user(char* username) {
    char sql[MAX_STRING_LENGTH_SQL] = "select count(*) as count from (select * from users where username = '";
    strcat(sql, username);
    strcat(sql, "')");
    int count = 0;
    select_query(sql, on_count_rows, &count);
    return count;
}



int is_exists_friend_request(char* sender, char* receiver) {
    char sender_string[MAX_STRING_LENGTH_TOKEN] = "";
    char receiver_string[MAX_STRING_LENGTH_TOKEN] = "";

    sprintf(sender_string, "%d", get_user_id(sender));
    sprintf(receiver_string, "%d", get_user_id(receiver));

    char sql[MAX_STRING_LENGTH_SQL] = "select count(*) as count from (select * from friend_requests where sender = '";
    strcat(sql, sender_string);
    strcat(sql, "' AND receiver = '");
    strcat(sql, receiver_string);
    strcat(sql, "')");
    int count = 0;
    select_query(sql, on_count_rows, &count);
    return count;
}

char* get_code_402(char* sender) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "402|");
    strcat(code, sender);
    return code;
}

char* get_code_401(char* success_message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "401|");
    strcat(code, success_message);
    return code;
}

char* get_code_403(char* error_message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "403|");
    strcat(code, error_message);
    return code;
}

void send_code_success_401(int client, char* message) {
    send_message(client, get_code_401(message));
}

void send_code_error_403(int client, char* message) {
    send_message(client, get_code_403(message));
}

void send_code_205(int client) {
    // respond code to client
	char* code_205 = get_code_205("Logging in is failed");
	send_message(client, code_205);
	free(code_205);
}

void send_code_201(int client) {
    // respond code to client
	char* code_201 = get_code_201();
	send_message(client, code_201);
	free(code_201);
}

char* get_code_202(char* friend_username, int status) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "202|");
    strcat(code, friend_username);
    strcat(code, "|");
    if(status == 0) {
        strcat(code, "0");
    } else {
        strcat(code, "1");
    }
    return code;
}

void send_code_202(int client, char* friend_username, int status) {
    char* code = get_code_202(friend_username, status);
    send_message(client, code);
    free(code);
}

int on_get_friend(void* result, int num_cols, char** row, char** cols) {
    Friend* friend = (Friend*)result;
    if(strcmp(cols[0], "username") == 0) {
		strcpy(friend->username, row[0]);
	} else {
		strcpy(friend->username, "");
	}
    if(strcmp(cols[1], "client") == 0) {
		int client = atoi(row[1]);
        if(client == -1) {
            friend->status = 0;
        } else {
            friend->status = 1;
        }
	} else {
		friend->status = 0;
	}
    send_code_202(friend->of_user, friend->username, friend->status);
    return 0;
}

void send_friend_list_to(char* username) {
    int client = get_client_socket(username);
    if(client == -1) return;
    int user_id = get_user_id(username);
    char user_id_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id_string, "%d", user_id);
    char sql[MAX_STRING_LENGTH_SQL] = "select username, client from friends inner join users on friends.friend = users.user_id where user = ";
    strcat(sql, user_id_string);
    Friend friend;
    friend.of_user = client;
    select_query(sql, on_get_friend, &friend);
}

void send_friend_info_to(char* username, char* friend) {
    int client = get_client_socket(username);
    if(client == -1) return;
    int user_id = get_user_id(username);
    char user_id_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id_string, "%d", user_id);
    char sql[MAX_STRING_LENGTH_SQL] = "select username, client from friends inner join users on friends.friend = users.user_id where user = ";
    strcat(sql, user_id_string);
    strcat(sql, " and username = '");
    strcat(sql, friend);
    strcat(sql, "'");
    Friend f;
    f.of_user = client;
    select_query(sql, on_get_friend, &f);
}

int on_get_invitation(void* result, int num_cols, char** row, char** cols) {
    Invitation* i = (Invitation*)result;
    if(strcmp(cols[0], "username") == 0) {
		strcpy(i->username, row[0]);
	} else {
		strcpy(i->username, "");
	}
    char* code_203 = get_code_203(i->username);
    send_message(i->to_user, code_203);
    free(code_203);
    return 0;
}

void send_invitations_to(char* username) {
    int client = get_client_socket(username);
    if(client == -1) return;
    int user_id = get_user_id(username);
    char user_id_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id_string, "%d", user_id);
    char sql[MAX_STRING_LENGTH_SQL] = "select username from friend_requests inner join users on friend_requests.sender = users.user_id where receiver = ";
    strcat(sql, user_id_string);
    Invitation invitation;
    invitation.to_user = client;
    select_query(sql, on_get_invitation, &invitation);
}

char* get_code_204(char* me) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "204|");
    strcat(code, me);
    return code;
}

int on_get_friend_to_notify_online(void* result, int num_cols, char** row, char** cols) {
    Friend* friend = (Friend*)result;
    if(strcmp(cols[0], "client") == 0) {
		friend->client = atoi(row[0]);
        char* code = get_code_204(friend->of_username);
        send_message(friend->client, code);
        free(code);
	} else {
		strcpy(friend->username, "");
	}
    return 0;
}

void notify_online_to_friend_of_user(char* username) {
    int client = get_client_socket(username);
    if(client == -1) return;
    int user_id = get_user_id(username);
    char user_id_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id_string, "%d", user_id);
    char sql[MAX_STRING_LENGTH_SQL] = "select client from friends inner join users on friends.friend = users.user_id where user = ";
    strcat(sql, user_id_string);
    Friend friend;
    strcpy(friend.of_username, username);
    select_query(sql, on_get_friend_to_notify_online, &friend);
}

char* get_code_302(char* username) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "302|");
    strcat(code, username);
    return code;
}

int on_get_friend_to_notify_offline(void* result, int num_cols, char** row, char** cols) {
    Friend* friend = (Friend*)result;
    if(strcmp(cols[0], "client") == 0) {
		friend->client = atoi(row[0]);
        char* code = get_code_302(friend->of_username);
        send_message(friend->client, code);
        free(code);
	} else {
		strcpy(friend->username, "");
	}
    return 0;
}

void notify_offline_to_friend_of_user(char* username) {
    int client = get_client_socket(username);
    if(client == -1) return;
    int user_id = get_user_id(username);
    char user_id_string[MAX_STRING_LENGTH_TOKEN] = "";
    sprintf(user_id_string, "%d", user_id);
    char sql[MAX_STRING_LENGTH_SQL] = "select client from friends inner join users on friends.friend = users.user_id where user = ";
    strcat(sql, user_id_string);
    Friend friend;
    strcpy(friend.of_username, username);
    select_query(sql, on_get_friend_to_notify_offline, &friend);
}

int remove_friend_in_database(char* username, char* friend_username) {
    int user_id = get_user_id(username);
    char* user_id_string = (char*) malloc(MAX_STRING_LENGTH_TOKEN);
    sprintf(user_id_string, "%d", user_id);

    int friend_id = get_user_id(friend_username);
    char* friend_id_string = (char*) malloc(MAX_STRING_LENGTH_TOKEN);
    sprintf(friend_id_string, "%d", friend_id);

    char sql[MAX_STRING_LENGTH_SQL] = "delete from friends where user = ";
    strcat(sql, user_id_string);
    strcat(sql, " and friend = ");
    strcat(sql, friend_id_string);

    return insert_query(sql);
}

char* get_code_801(char* success_message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "801|");
    strcat(code, success_message);
    return code;
}

void send_code_801(int sender_socket, char* success_message) {
    char* code = get_code_801(success_message);
    send_message(sender_socket, code);
    free(code);
}

char* get_code_802(char* sender, char* message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "802|");
    strcat(code, sender);
    strcat(code, "|");
    strcat(code, message);
    return code;
}

void send_code_802(int sender_socket, int receiver_socket, char* text_message) {
    char* sender = get_client_username(sender_socket);
    char* code = get_code_802(sender, text_message);
    send_message(receiver_socket, code);
    free(code);
    free(sender);
}

char* get_code_803(char* receiver, char* error_message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "803|");
    strcat(code, receiver);
    strcat(code, "|");
    strcat(code, error_message);
    return code;
}

void send_code_803(int sender_socket, char* receiver, char* error_message) {
    char* code = get_code_803(receiver, error_message);
    send_message(sender_socket, code);
    free(code);
}

char* get_code_701(char* friend_username, char* message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "701|");
    strcat(code, friend_username);
    strcat(code, "|");
    strcat(code, message);
    return code;
}

void send_code_701(int client, char* friend_username, char* message) {
    char* code = get_code_701(friend_username, message);
    send_message(client, code);
    free(code);
}

char* get_code_702(char* username) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "702|");
    strcat(code, username);
    return code;
}

void send_code_702(char* username, char* friend_username) {
    char* code = get_code_702(username);
    int client = get_client_socket(friend_username);
    if(client == -1) {
        return;
    }
    send_message(client, code);
    free(code);
}

char* get_code_703(char* friend_username, char* message) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "703|");
    strcat(code, friend_username);
    strcat(code, "|");
    strcat(code, message);
    return code;
}

void send_code_703(int client, char* friend_username, char* message) {
    char* code = get_code_703(friend_username, message);
    send_message(client, code);
    free(code);
}

char* get_code_203(char* sender) {
    char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(code, "203|");
    strcat(code, sender);
    return code;
}
void send_code_203(int client, char* sender) {
    char* code = get_code_203(sender);
    send_message(client, code);
    free(code);
}

char* current_date_time() {
    char* time_string = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(time_string, "");

    time_t current_time;
    current_time = time(NULL);
    if (current_time == ((time_t)-1)) {
        return "*";
    }

    time_string = ctime(&current_time);
    if (time_string == NULL) {
        return "*";
    }

    time_string[strlen(time_string)-1] = 0;
    return time_string;
}

void append_int(char* string, int integer) {
    char *integer_string = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(integer_string, "");
    sprintf(integer_string, "%d", integer);
    strcat(string, integer_string);
}

void log_info(char* string) {
    char* time = current_date_time();
    printf("%s\n -> ", time);
    printf("%s\n", string);
}