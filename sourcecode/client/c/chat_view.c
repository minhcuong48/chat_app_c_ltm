#include <chat_view.h>

ChatView* chat_view_new() {
    ChatView *this = (ChatView*) malloc(sizeof(ChatView));

    this->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    this->main_alignment = gtk_alignment_new(0, 0, 1, 1);
    this->main_container = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    this->scrolled_window = gtk_scrolled_window_new(NULL, NULL);
    this->text_view = gtk_text_view_new();
    this->bottom_container = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 8);
    this->entry = gtk_entry_new();
    this->send_button = gtk_button_new_with_label("Send");

    gtk_container_add(GTK_CONTAINER(this->window), this->main_alignment);
    gtk_container_add(GTK_CONTAINER(this->main_alignment), this->main_container);
    gtk_box_pack_start(GTK_BOX(this->main_container), this->scrolled_window, TRUE, TRUE, 8);
    gtk_container_add(GTK_CONTAINER(this->scrolled_window), this->text_view);
    gtk_box_pack_start(GTK_BOX(this->main_container), this->bottom_container, FALSE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(this->bottom_container), this->entry, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(this->bottom_container), this->send_button, TRUE, TRUE, 8);

    gtk_widget_set_size_request(this->window, 300, 300);
    gtk_text_view_set_editable(GTK_TEXT_VIEW(this->text_view), FALSE);

    return this;
}

void chat_view_show(ChatView *view) {
    gtk_widget_show_all(GTK_WIDGET(view->window));
}

void chat_view_append_line(ChatView* this, char* sender, char* message) {
    GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(this->text_view));
    GtkTextIter iter;
    gtk_text_buffer_get_end_iter(buffer, &iter);
    gtk_text_buffer_insert(buffer, &iter, sender, -1);
    gtk_text_buffer_insert(buffer, &iter, ": ", -1);
    gtk_text_buffer_insert(buffer, &iter, message, -1);
    gtk_text_buffer_insert(buffer, &iter, "\n", -1);
    chat_view_scroll_to_bottom(this);
}

void chat_view_scroll_to_bottom(ChatView* this) {
    GtkTextView* text_view = (GtkTextView*)(this->text_view);
    GtkAdjustment* adj = gtk_text_view_get_vadjustment(text_view);
    gdouble value = gtk_adjustment_get_upper(adj);
    gtk_adjustment_set_value(adj, value);
}