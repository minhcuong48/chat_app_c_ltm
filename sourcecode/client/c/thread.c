#include <thread.h>
#include <constant.h>
#include <stdlib.h>
#include <unistd.h>
#include <helper.h>
#include <string.h>
#include <main_controller.h>

// This func will be executed for get message from server
static void* on_waitting_thread(void* args);

Thread* thread_new(int server, MainController* main_controller, ChatManager* chat_manager) {
    Thread* this = (Thread*) malloc(sizeof(Thread));
	this->server = server;
	this->main_controller = main_controller;
	this->chat_manager = chat_manager;
	pthread_t thread_id;
	pthread_create(&thread_id, NULL, &on_waitting_thread, this);
    this->thread_id = thread_id;
    return this;
}

static gboolean g_quick_message(gpointer data) {
	char* message = (char*) data;
	quick_message(message);
	return FALSE;
}

static gboolean g_main_view_show_main(gpointer data) {
	Thread* thread = (Thread*) data;
	MainView* main_view = thread->main_controller->main_view;
	main_view_show_main(main_view);
	return FALSE;
}

static gboolean g_main_view_append_friend(gpointer data) {
	Bundle *b = (Bundle*) data;
	Thread* thread = (Thread*)b->data[0];
	char* username = (char*)b->data[1];
	char* status = (char*)b->data[2];

	MainView* main_view = thread->main_controller->main_view;
	main_view_append_friend(main_view, username, status);
	return FALSE;
}

static gboolean g_main_view_append_invitation(gpointer data) {
	Bundle *b = (Bundle*) data;
	Thread* thread = (Thread*)b->data[0];
	char* invitation = (char*)b->data[1];

	MainView* main_view = thread->main_controller->main_view;
	main_view_append_invitation(main_view, invitation);
	return FALSE;
}

static gboolean g_chat_manager_pass_message(gpointer data) {
	Bundle *b = (Bundle*) data;
	Thread* thread = (Thread*)b->data[0];
	char* sender = (char*)b->data[1];
	char* message = (char*)b->data[2];
	ChatManager* chat_manager = thread->chat_manager;
	chat_manager_pass_message(chat_manager, sender, message);

	return FALSE;
}

static gboolean g_main_view_delete_invitation(gpointer data) {
	Bundle *b = (Bundle*) data;
	Thread* thread = (Thread*)b->data[0];
	char* invitation = (char*)b->data[1];
	MainController* main_controller = thread->main_controller;
	MainView* main_view = main_controller->main_view;
	main_view_delete_invitation(main_view, invitation);
	return FALSE;
}

static gboolean g_main_view_delete_friend(gpointer data) {
	Bundle *b = (Bundle*) data;
	Thread* thread = (Thread*)b->data[0];
	char* username = (char*)b->data[1];
	MainController* main_controller = thread->main_controller;
	MainView* main_view = main_controller->main_view;
	main_view_delete_friend(main_view, username);

	return FALSE;
}

static gboolean g_main_view_set_friend_status(gpointer data) {
	Bundle *b = (Bundle*) data;
	Thread* thread = (Thread*)b->data[0];
	char* username = (char*)b->data[1];
	char* status = (char*)b->data[2];
	MainController* main_controller = thread->main_controller;
	MainView* main_view = main_controller->main_view;
	main_view_set_friend_status(main_view, username, status);
	return FALSE;
}

static void handle_message(Thread* thread, char* message) {
	char* header = get_header(message);
	List l = extract_message_to_list(message);
	if(strcmp(header, "101") == 0) {
		char* message = get_node_string(2, l);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "102") == 0) {
		char* message = get_node_string(2, l);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "201") == 0) {
		char* message = get_node_string(2, l);
		g_idle_add(g_main_view_show_main, thread);
	}
	else if(strcmp(header, "202") == 0) {
		char* username = get_node_string(2, l);
		char* status = get_node_string(3, l);
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, username);
		bundle_add(b, status);
		g_idle_add(g_main_view_append_friend, b);
	}
	else if(strcmp(header, "502") == 0) {
		char* username = get_node_string(2, l);
		char* status = get_node_string(3, l);
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, username);
		bundle_add(b, status);
		g_idle_add(g_main_view_append_friend, b);

		char* message = (char*) malloc(MAX_STRING_LENGTH_TOKEN);
		strcpy(message, username);
		strcpy(message, " accepted your friend request!");
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "204") == 0) {
		char* username = get_node_string(2, l);
		char* status = (char*) malloc(MAX_STRING_LENGTH_TOKEN);
		strcpy(status, "1");
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, username);
		bundle_add(b, status);
		g_idle_add(g_main_view_set_friend_status, b);
	}
	else if(strcmp(header, "205") == 0) {
		char* message = get_node_string(2, l);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "302") == 0) {
		char* username = get_node_string(2, l);
		char* status = (char*) malloc(MAX_STRING_LENGTH_TOKEN);
		strcpy(status, "0");
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, username);
		bundle_add(b, status);
		g_idle_add(g_main_view_set_friend_status, b);
	}
	else if(strcmp(header, "401") == 0) {
		char* message = get_node_string(2, l);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "203") == 0) {
		char* invitation = get_node_string(2, l);
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, invitation);
		g_idle_add(g_main_view_append_invitation, b);
	}
	else if(strcmp(header, "402") == 0) {
		char* invitation = get_node_string(2, l);
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, invitation);
		g_idle_add(g_main_view_append_invitation, b);
		//g_timeout_add(250, g_main_view_append_invitation, b);

		char* message = (char*) malloc(MAX_STRING_LENGTH_TOKEN);
		strcpy(message, "You have got invitation from ");
		strcat(message, invitation);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "403") == 0) {
		char* message = get_node_string(2, l);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "501") == 0) {
		char* invitation = get_node_string(2, l);
		char* message = get_node_string(3, l);
		g_idle_add(g_quick_message, message);

		Bundle* b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, invitation);
		g_idle_add(g_main_view_delete_invitation, b);
	}
	else if(strcmp(header, "503") == 0) {
		char* requester = get_node_string(2, l);
		char* _message = (char*) malloc(MAX_STRING_LENGTH_PROTOCOL);
		strcpy(_message, "Accepting invitation from ");
		strcat(_message, requester);
		strcat(_message, " is failed!");
		g_idle_add(g_quick_message, _message);
	}
	else if(strcmp(header, "601") == 0) {
		char* invitation = get_node_string(2, l);
		char* message = get_node_string(3, l);
		g_idle_add(g_quick_message, message);

		Bundle* b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, invitation);
		g_idle_add(g_main_view_delete_invitation, b);
	}
	else if(strcmp(header, "602") == 0) {
		char* message = get_node_string(2, l);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "603") == 0) {
		char* requester = get_node_string(2, l);
		char* _message = (char*) malloc(MAX_STRING_LENGTH_PROTOCOL);
		strcpy(_message, "Deleting invitation from ");
		strcat(_message, requester);
		strcat(_message, " is failed!");
		g_idle_add(g_quick_message, _message);
	}
	else if(strcmp(header, "701") == 0) {
		char* friend_username = get_node_string(2, l);
		char* message = get_node_string(3, l);
		g_idle_add(g_quick_message, message);

		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, friend_username);
		g_idle_add(g_main_view_delete_friend, b);
	}
	else if(strcmp(header, "702") == 0) {
		char* friend_username = get_node_string(2, l);

		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, friend_username);
		g_idle_add(g_main_view_delete_friend, b);
	}
	else if(strcmp(header, "703") == 0) {
		char* friend_username = get_node_string(2, l);
		char* message = get_node_string(3, l);
		g_idle_add(g_quick_message, message);
	}
	else if(strcmp(header, "802") == 0) {
		char* sender = get_node_string(2, l);
		char* message = get_node_string(3, l);
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, sender);
		bundle_add(b, message);
		g_idle_add(g_chat_manager_pass_message, b);
	}
	else if(strcmp(header, "803") == 0) {
		char* receiver = get_node_string(2, l);
		char* message = get_node_string(3, l);
		Bundle *b = bundle_new();
		bundle_add(b, thread);
		bundle_add(b, receiver);
		bundle_add(b, message);
		g_idle_add(g_chat_manager_pass_message, b);
	}
}

// args is (int*) server
static void* on_waitting_thread(void* data) {
	Thread* thread = (Thread*) data;
	int server = thread->server;
	ssize_t message_length;
	char message[MAX_STRING_LENGTH_PROTOCOL];
	bzero(message, MAX_STRING_LENGTH_PROTOCOL);
	strcpy(message, "");
	// send_message(server, "LGIN|gminhcuong|123456");
	while ((message_length = recv(server, &message, MAX_STRING_LENGTH_PROTOCOL, 0)) > 0) {
		char* copy_message = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
		strcpy(copy_message, message);
		// when got message here
		log_info(copy_message);
		handle_message(thread, copy_message);
		bzero(message, MAX_STRING_LENGTH_PROTOCOL);
		strcpy(message, "");
	}

	// 5a Check recv error
	if (message_length < 0) {
		log_info("recv() from server error");
		return (void*)1;
	}

	// 5b Check client close
	if (message_length == 0) {
		log_info("Server closed");
		return (void*)0;
	}
}