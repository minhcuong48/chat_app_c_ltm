#include <main_view.h>
#include <gtk/gtk.h>

MainView* main_view_new() {
    MainView* this = (MainView*)malloc(sizeof(MainView));
    this->current_username = NULL;

    this->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    this->main_alignment = gtk_alignment_new(0.5, 0.3, 0, 0);
    this->main_container = gtk_box_new(GTK_ORIENTATION_VERTICAL, 8.0);

    this->username_label = gtk_label_new("Username");
    this->username_entry = gtk_entry_new();

    this->password_label = gtk_label_new("Password");
    this->password_entry = gtk_entry_new();

    this->buttons_container = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 8.0);
    this->login_button = gtk_button_new_with_label("Log in");
    this->signup_button = gtk_button_new_with_label("Sign up");

    // after login
    this->add_friend_button = gtk_button_new_with_label("Add Friend");

    // init invitations tree view
    this->invitation_tree_store = gtk_tree_store_new(INVITATION_NUM_COLUMNS, G_TYPE_STRING);
    this->invitation_tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(this->invitation_tree_store));
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn *column;
    column = gtk_tree_view_column_new_with_attributes("Invitations", renderer, "text", INVITATION_COLUMN, NULL);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(this->invitation_tree_view), column);

    // init friends table view
    this->friend_tree_store = gtk_tree_store_new(FRIEND_NUM_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
    this->friend_tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(this->friend_tree_store));
    column = gtk_tree_view_column_new_with_attributes("Friend", renderer, "text", FRIEND_COLUMN, NULL);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(this->friend_tree_view), column);

    column = gtk_tree_view_column_new_with_attributes("Status", renderer, "text", STATUS_COLUMN, NULL);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(this->friend_tree_view), column);

    // init invitation buttons
    this->accept_button = gtk_button_new_with_label("Accept");
    this->decline_button = gtk_button_new_with_label("Decline");
    this->invitation_buttons_container = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 8.0);

    // init delete friend button
    this->delete_friend_button = gtk_button_new_with_label("Delete friend");

    // end after login

    gtk_container_add(GTK_CONTAINER(this->window), this->main_alignment);
    gtk_container_add(GTK_CONTAINER(this->main_alignment), this->main_container);
    gtk_box_pack_start(GTK_BOX(this->main_container), this->username_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(this->main_container), this->username_entry, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(this->main_container), this->password_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(this->main_container), this->password_entry, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(this->main_container), this->buttons_container, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(this->buttons_container), this->login_button, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(this->buttons_container), this->signup_button, TRUE, TRUE, 0);

    gtk_entry_set_visibility(this->password_entry, FALSE);
    gtk_widget_set_size_request(this->window, 300, 600);
    gtk_widget_set_size_request(this->main_container, 250, 0);
    gtk_label_set_xalign(this->username_label, 0);
    gtk_label_set_xalign(this->password_label, 0);

    gtk_window_set_title(GTK_WINDOW(this->window), "Log in");

    return this;
}

void main_view_show(MainView* view) {
    gtk_widget_show_all(GTK_WIDGET(view->window));
}

void main_view_show_main(MainView *view) {
    // display current username as title
    if (view->current_username != NULL) {
        gtk_window_set_title(GTK_WINDOW(view->window), view->current_username);
    } else {
        gtk_window_set_title(GTK_WINDOW(view->window), "Main View");
    }
    // destroy main alignment
    gtk_widget_destroy(view->main_alignment);
    // init view objects
    view->main_alignment = gtk_alignment_new(0, 0, 1, 0);
    view->main_container = gtk_box_new(GTK_ORIENTATION_VERTICAL, 8);
    view->add_friend_container = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    view->friend_username_entry = gtk_entry_new();

    // setup layout
    gtk_container_add(GTK_CONTAINER(view->window), view->main_alignment);
    gtk_container_add(GTK_CONTAINER(view->main_alignment), view->main_container);
    gtk_box_pack_start(GTK_BOX(view->main_container), view->add_friend_container, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(view->add_friend_container), view->friend_username_entry, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(view->add_friend_container), view->add_friend_button, FALSE, FALSE, 8);
    gtk_box_pack_start(GTK_BOX(view->main_container), view->invitation_tree_view, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(view->main_container), view->invitation_buttons_container, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(view->invitation_buttons_container), view->accept_button, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(view->invitation_buttons_container), view->decline_button, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(view->main_container), view->friend_tree_view, TRUE, TRUE, 8);
    gtk_box_pack_start(GTK_BOX(view->main_container), view->delete_friend_button, TRUE, TRUE, 8);

    gtk_widget_set_size_request(view->window, 300, 600);

    
    gtk_widget_show_all(view->window);
}

void main_view_append_friend(MainView* main_view, char* username, char* status) {
    GtkTreeStore* friend_tree_store = main_view->friend_tree_store;
    GtkTreeIter iter;
    gtk_tree_store_append(friend_tree_store, &iter, NULL);
    gtk_tree_store_set(friend_tree_store, &iter, FRIEND_COLUMN, username, STATUS_COLUMN, status, -1);
}

void main_view_append_invitation(MainView* main_view, char* invitation) {
    GtkTreeModel* invitation_tree_model = gtk_tree_view_get_model(GTK_TREE_VIEW(main_view->invitation_tree_view));
    GtkTreeStore* invitation_tree_store = GTK_TREE_STORE(invitation_tree_model);
    GtkTreeIter iter;
    gtk_tree_store_append(invitation_tree_store, &iter, NULL);
    gtk_tree_store_set(invitation_tree_store, &iter, INVITATION_COLUMN, invitation, -1);
}

void main_view_delete_invitation(MainView* main_view, char* invitation) {
    GtkTreeStore* invitation_tree_store = main_view->invitation_tree_store;
    GtkTreeView* invitation_tree_view = main_view->invitation_tree_view;
    GtkTreeIter iter;
    GtkTreeModel *model = gtk_tree_view_get_model(invitation_tree_view);
    gtk_tree_model_get_iter_first(model, &iter);
    char* _invitation;
    gtk_tree_model_get(model, &iter, INVITATION_COLUMN, &_invitation, -1);
    if(strcmp(_invitation, invitation) == 0) {
        gtk_tree_store_remove(invitation_tree_store, &iter);
        return;
    }
    while(gtk_tree_model_iter_next(model, &iter) == TRUE) {
        gtk_tree_model_get(model, &iter, INVITATION_COLUMN, &_invitation, -1);
        if(strcmp(_invitation, invitation) == 0) {
            gtk_tree_store_remove(invitation_tree_store, &iter);
            return;
        }
    }
}

void main_view_set_friend_status(MainView* main_view, char* username, char* status) {
    GtkTreeStore* friend_tree_store = main_view->friend_tree_store;
    GtkTreeView* friend_tree_view = main_view->friend_tree_view;
    GtkTreeIter iter;
    GtkTreeModel *model = gtk_tree_view_get_model(friend_tree_view);
    gtk_tree_model_get_iter_first(model, &iter);
    char* _username;
    char* _status;
    gtk_tree_model_get(model, &iter, FRIEND_COLUMN, &_username, STATUS_COLUMN, &_status, -1);
    if(strcmp(_username, username) == 0) {
        gtk_tree_store_set(friend_tree_store, &iter, STATUS_COLUMN, status, -1);
        return;
    }
    while(gtk_tree_model_iter_next(model, &iter) == TRUE) {
        gtk_tree_model_get(model, &iter, FRIEND_COLUMN, &_username, STATUS_COLUMN, &_status, -1);
        if(strcmp(_username, username) == 0) {
            gtk_tree_store_set(friend_tree_store, &iter, STATUS_COLUMN, status, -1);
            return;
        }
    }
}

void main_view_delete_friend(MainView* main_view, char* username) {
    GtkTreeStore* friend_tree_store = main_view->friend_tree_store;
    GtkTreeView* friend_tree_view = main_view->friend_tree_view;
    GtkTreeIter iter;
    GtkTreeModel *model = gtk_tree_view_get_model(friend_tree_view);
    gtk_tree_model_get_iter_first(model, &iter);
    char* _username;
    char* _status;
    gtk_tree_model_get(model, &iter, FRIEND_COLUMN, &_username, STATUS_COLUMN, &_status, -1);
    if(strcmp(_username, username) == 0) {
        gtk_tree_store_remove(friend_tree_store, &iter);
        return;
    }
    while(gtk_tree_model_iter_next(model, &iter) == TRUE) {
        gtk_tree_model_get(model, &iter, FRIEND_COLUMN, &_username, STATUS_COLUMN, &_status, -1);
        if(strcmp(_username, username) == 0) {
            gtk_tree_store_remove(friend_tree_store, &iter);
            return;
        }
    }
}