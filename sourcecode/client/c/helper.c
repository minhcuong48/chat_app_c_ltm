#include <helper.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <constant.h>
#include <linked_list.h>
#include <gtk/gtk.h>

int send_message_to_server(char* message) {
	char tmp[MAX_STRING_LENGTH_PROTOCOL] = "";
	strcpy(tmp, message);
	ssize_t sent_length;
	sent_length = send(server, message, strlen(message), 0);
	if((int)sent_length > 0) {
        char _info[MAX_STRING_LENGTH_PROTOCOL];
        sprintf(_info, "Sent \"%s\"", message);
        log_info(_info);
    }
	return (int)sent_length;
}

int send_message(int server, char* message) {
	char tmp[MAX_STRING_LENGTH_PROTOCOL] = "";
	strcpy(tmp, message);
	ssize_t sent_length;
	sent_length = send(server, message, strlen(message), 0);
	if((int)sent_length > 0) {
        char _info[MAX_STRING_LENGTH_PROTOCOL];
        sprintf(_info, "Sent \"%s\"", message);
        log_info(_info);
    }
	return (int)sent_length;
}

int isEndWithSeparator(const char* message) {
	if(message[strlen(message) - 1] == '|') {
		return 1;
	}
	return 0;
}

char* get_header(char* message) {
	List l = extract_message_to_list(message);
	char* header = (char*)malloc(MAX_STRING_LENGTH_TOKEN);
	strcpy(header, get_node_string(1, l));
	return header;
}

int count_message_tokens(char* message) {
	List list = extract_message_to_list(message);
	return count_nodes(list);
}

List extract_message_to_list(char* message) {
	List list = create_list();
	int len = strlen(message);
	int from = 0, to = 0, i = 0;
	char separator[2] = "|";
	for(i=0; i<len; i++) {
		if(i == 0 || message[i-1] == separator[0]) {
			from = i;
		}
		if (i == len-1 || message[i+1] == separator[0]) {
			to = i;
			char* token = sub_string(message, from, to);
			add_node(&list, create_node(token));
		}
	}
	return list;
}

char* sub_string(char* source, int from, int to) {
	int source_len = strlen(source);
	int i = 0;
	int dest_len = to - from + 1;
	char* dest = malloc(dest_len + 1);
	for (i = 0; i < dest_len; i++) {
		dest[i] = source[i + from];
	}
	dest[i] = 0;
	return dest;
}

char* current_date_time() {
    char* time_string = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(time_string, "");

    time_t current_time;
    current_time = time(NULL);
    if (current_time == ((time_t)-1)) {
        return "*";
    }

    time_string = ctime(&current_time);
    if (time_string == NULL) {
        return "*";
    }

    time_string[strlen(time_string)-1] = 0;
    return time_string;
}

void append_int(char* string, int integer) {
    char *integer_string = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
    strcpy(integer_string, "");
    sprintf(integer_string, "%d", integer);
    strcat(string, integer_string);
}

void log_info(char* string) {
    char* time = current_date_time();
    printf("%s\n -> ", time);
    printf("%s\n", string);
}

int connect_server(char* server_ip, int port) {
	// 1 Tao socket
	int server = socket(AF_INET, SOCK_STREAM, 0);
	if (server < 0) {
		log_info("Opening socket to server is failed");
		return -1;
	}

	// 2 Connect
	// 2.1 Tao server_address
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	// cach 2 su dung inet_addr. Method nay khong duoc khuyen khich
	//server_address.sin_addr.s_addr = inet_addr(server_ip);
	inet_aton(server_ip, &(server_address.sin_addr));
	server_address.sin_port = htons(port);

	// 2.2 Connect
	int connect_result = connect(server, (struct sockaddr*)&server_address, sizeof(server_address));
	if (connect_result < 0) {
		log_info("Connecting to server is failed");
	}
	return server;
}

static void message_dialog_destroy(GtkWidget* dialog, gpointer data) {
	gtk_widget_destroy(GTK_WIDGET(dialog));
}

void quick_message(gchar *message) {
 	GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
	GtkMessageDialog* dialog = gtk_message_dialog_new(NULL,
									flags,
									GTK_MESSAGE_ERROR,
									GTK_BUTTONS_CLOSE,
									"%s",
									message);

	g_signal_connect(dialog, "response",
							G_CALLBACK(message_dialog_destroy),
							dialog);
	gtk_window_set_modal(GTK_WINDOW(dialog), FALSE);
	gtk_widget_show_all(GTK_WIDGET(dialog));
}

// for bundle
Bundle* bundle_new() {
	Bundle* b = (Bundle*) malloc(sizeof(Bundle));
	b->size = 0;
	return b;
}

void bundle_add(Bundle* b, void* item) {
	int count = b->size;
	b->size++;
	b->data[count] = item;
}