#include <main_controller.h>
#include <gtk/gtk.h>
#include <protocol.h>
#include <helper.h>

static void on_login_button_clicked(GtkButton* button, gpointer data) {
    MainController *main_controller = (MainController*)data;
    MainView* main_view = main_controller->main_view;
    GtkEntry* username_entry = (GtkEntry*)(main_view->username_entry);
    GtkEntry* password_entry = (GtkEntry*)(main_view->password_entry);

    gchar* username = gtk_entry_get_text(username_entry);
    gchar* password = gtk_entry_get_text(password_entry);

    // save current username
    main_view->current_username = (char*) malloc(MAX_STRING_LENGTH_TOKEN);
    strcpy(main_view->current_username, username);

    char* code_login = get_code_login(username, password);
    send_message_to_server(code_login);
    free(code_login);
}

static void on_destroy_window(GtkWidget *object, gpointer user_data) {
    gtk_main_quit();
}

static void on_signup_button_clicked(GtkButton* button, gpointer data) {
    MainController *main_controller = (MainController*)data;
    MainView* main_view = main_controller->main_view;
    GtkEntry* username_entry = (GtkEntry*)(main_view->username_entry);
    GtkEntry* password_entry = (GtkEntry*)(main_view->password_entry);

    gchar* username = gtk_entry_get_text(username_entry);
    gchar* password = gtk_entry_get_text(password_entry);
    char* code_signup = get_code_signup(username,password);
    send_message_to_server(code_signup);
    free(code_signup);
}

static void on_add_friend_button_clicked(GtkButton* button, gpointer data) {
    MainController *main_controller = (MainController*)data;
    MainView* main_view = main_controller->main_view;
    GtkEntry* friend_username_entry = main_view->friend_username_entry;
    gchar* text = gtk_entry_get_text(friend_username_entry);
    if(text == NULL || strcmp(text, "") == 0) {
        return;
    }
    char* code_freq = get_code_friend_request(text);
    send_message_to_server(code_freq);
    free(code_freq);
    gtk_entry_set_text(friend_username_entry, "");
}

static void on_friend_row_double_clicked(GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data) {
    MainController *main_controller = (MainController*)data;
    MainView* main_view = main_controller->main_view;
    ChatManager* chat_manager = main_controller->chat_manager;
    
    char *friend_username;
    GtkTreeIter iter;
    GtkTreeModel *model = gtk_tree_view_get_model(tree_view);
    gtk_tree_model_get_iter(model, &iter, path);
    gtk_tree_model_get(model, &iter, FRIEND_COLUMN, &friend_username, -1);
    ChatController* chat_controller;
    if((chat_controller = chat_manager_get(chat_manager, friend_username)) == NULL) {
        chat_manager_add(chat_manager, friend_username);
    } else {
        gtk_widget_show_all(chat_controller->chat_view->window);
    }
}

// static void on_invitation_row_double_clicked(GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data) {
    // char *invitation;
    // GtkTreeIter iter;
    // GtkTreeModel *model = gtk_tree_view_get_model(tree_view);
    // gtk_tree_model_get_iter(model, &iter, path);
    // gtk_tree_model_get(model, &iter, INVITATION_COLUMN, &invitation, -1);
// }

static void on_accept_button_clicked(GtkButton* button, gpointer data) {
    MainController *main_controller = (MainController*)data;
    MainView* main_view = main_controller->main_view;
    ChatManager* chat_manager = main_controller->chat_manager;
    
    GtkTreeSelection *invitation_tree_selection = gtk_tree_view_get_selection(main_view->invitation_tree_view);
    GtkTreeModel *invitation_tree_model;
    GtkTreeIter iter;
    if(gtk_tree_selection_get_selected (invitation_tree_selection,
        &invitation_tree_model,
        &iter) == TRUE) {
            char* invitation_username;
            gtk_tree_model_get(invitation_tree_model, &iter, INVITATION_COLUMN, &invitation_username, -1);
            char* code_accr = get_code_accept_request(invitation_username);
            send_message_to_server(code_accr);
            free(code_accr);
    }
}

static void on_decline_button_clicked(GtkButton* button, gpointer data) {
    MainController *main_controller = (MainController*)data;
    MainView* main_view = main_controller->main_view;
    ChatManager* chat_manager = main_controller->chat_manager;
    
    GtkTreeSelection *invitation_tree_selection = gtk_tree_view_get_selection(main_view->invitation_tree_view);
    GtkTreeModel *invitation_tree_model;
    GtkTreeIter iter;
    if(gtk_tree_selection_get_selected(invitation_tree_selection,
        &invitation_tree_model,
        &iter) == TRUE) {
            char* invitation_username;
            gtk_tree_model_get(invitation_tree_model, &iter, INVITATION_COLUMN, &invitation_username, -1);
            char* code_decr = get_code_decline_request(invitation_username);
            send_message_to_server(code_decr);
            free(code_decr);
    }
}

static void on_delete_friend_button_clicked(GtkButton* button, gpointer data) {
    MainController *main_controller = (MainController*)data;
    MainView* main_view = main_controller->main_view;
    ChatManager* chat_manager = main_controller->chat_manager;
    
    GtkTreeSelection *friend_tree_selection = gtk_tree_view_get_selection(main_view->friend_tree_view);
    GtkTreeModel *friend_tree_model;
    GtkTreeIter iter;
    if(gtk_tree_selection_get_selected(friend_tree_selection,
        &friend_tree_model,
        &iter) == TRUE) {
            char* _username;
            gtk_tree_model_get(friend_tree_model, &iter, FRIEND_COLUMN, &_username, -1);
            char* code_delf = get_code_delete_friend(_username);
            send_message_to_server(code_delf);
            free(code_delf);
    }
}

static void setup_events(MainController* this) {
    MainView* main_view = this->main_view;

    g_signal_connect(G_OBJECT(main_view->window), "destroy", G_CALLBACK(on_destroy_window), NULL);

    GtkWidget* login_button = main_view->login_button;
    g_signal_connect(G_OBJECT(login_button), "clicked", G_CALLBACK(on_login_button_clicked), this);

    GtkWidget* signup_button = main_view->signup_button;
    g_signal_connect(G_OBJECT(signup_button), "clicked", G_CALLBACK(on_signup_button_clicked), this);

    GtkButton* add_friend_button = main_view->add_friend_button;
    g_signal_connect(G_OBJECT(add_friend_button), "clicked", G_CALLBACK(on_add_friend_button_clicked), this);

    GtkTreeView *friend_tree_view = main_view->friend_tree_view;
    g_signal_connect(G_OBJECT(friend_tree_view), "row-activated", G_CALLBACK(on_friend_row_double_clicked), this);

    // GtkTreeView *invitation_tree_view = main_view->invitation_tree_view;
    // g_signal_connect(G_OBJECT(invitation_tree_view), "row-activated", G_CALLBACK(on_invitation_row_double_clicked), this);

    GtkWidget *accept_button = main_view->accept_button;
    g_signal_connect(G_OBJECT(accept_button), "clicked", G_CALLBACK(on_accept_button_clicked), this);

    GtkWidget *decline_button = main_view->decline_button;
    g_signal_connect(G_OBJECT(decline_button), "clicked", G_CALLBACK(on_decline_button_clicked), this);

    GtkWidget *delete_friend_button = main_view->delete_friend_button;
    g_signal_connect(G_OBJECT(delete_friend_button), "clicked", G_CALLBACK(on_delete_friend_button_clicked), this);
}

MainController* main_controller_new(ChatManager* chat_manager) {
    MainController* this = (MainController*)malloc(sizeof(MainController));
    this->main_view = main_view_new();
    this->chat_manager = chat_manager;
    setup_events(this);
    return this;
}

void main_controller_start(MainController* this) {
    main_view_show(this->main_view);
}