/*
* @Author: GiapMinhCuong-20140539
* @Date:   2017-11-01 02:11:19
* @Last Modified by:   GiapMinhCuong-20140539
* @Last Modified time: 2017-11-02 01:11:30
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <pthread.h>
#include <helper.h>
#include <thread.h>
#include <main_controller.h>
#include <chat_controller.h>
#include <chat_manager.h>

static void* on_waitting_thread(void* args);

int server;

int main(int argc, char* argv[]) {
	// check arguments
	if(argc != 2) {
		printf("Type ./client <Server IP>\n");
		return 1;
	}

	char* server_ip = argv[1];

	server = connect_server(server_ip, PORT);
	if(server < 0) {
		char _info[MAX_STRING_LENGTH_PROTOCOL];
		sprintf(_info, "Cant connect to server at %s:%d", server_ip, PORT);
		log_info(_info);
		return 1;
	} else {
		char _info[MAX_STRING_LENGTH_PROTOCOL];
		sprintf(_info, "Connected to server at %s:%d", server_ip, PORT);
		log_info(_info);
	}

	gtk_init(&argc, &argv);

	// create chat manager
	ChatManager* chat_manager = chat_manager_new();

	// create UI in here
	MainController* main_controller = main_controller_new(chat_manager);
	main_controller_start(main_controller);

	// create thread for listening message from server
	Thread* thread = thread_new(server, main_controller, chat_manager);

	// end of main
	gtk_main();
	close(server);
	free(thread);
	log_info("Client closed");
	return 0;
}