#include <linked_list.h>

Node* create_head() {
	Node* node = (Node*)malloc(sizeof(Node));
	strcpy(node->string, "");
	node->next = NULL;
	return node;
}

List create_list() {
	List list;
	list.count = 0;
	list.current = 0;
	list.head = create_head();
	return list;
}

Node* create_node(char* string) {
	Node* node = (Node*)malloc(sizeof(Node));
	strcpy(node->string, string);
	node->next = NULL;
	return node;
}

Node* add_node(List* list, Node* node) {
	list->count += 1;
	Node* last_node = get_last_node(list);
	last_node->next = node;
	return node;
}

Node* get_last_node(List* list) {
	Node* tmp = list->head;
	while(tmp->next != NULL) {
		tmp = tmp->next;
	}
	return tmp;
}

Node* get_next_node(List* list) {
	Node* tmp = list->head;
	if (list->current < list->count) {
		list->current += 1;
		int i = 0;
		for(i=0; i < list->current; i++) {
			tmp = tmp->next;
		}
		return tmp;
	} else {
		list->current = 0;
	}
	return NULL;
}

int count_nodes(List list) {
	Node* tmp = list.head;
	int number = 0;
	while(tmp->next != NULL) {
		tmp = tmp->next;
		number += 1;
	}
	return number;
}

void print_list(List list) {
	Node* tmp = list.head;
	while(tmp->next != NULL) {
		tmp = tmp->next;
		printf("%s\n", tmp->string);
	}
}

char* get_node_string(int index, List list) {
	Node* tmp = list.head;
	int count = count_nodes(list);
	int i = 0;
	for(i=0; i<index; i++) {
		if (tmp->next != NULL) {
			tmp = tmp->next;
		} else {
			return NULL;
		}
	}
	char* result = malloc(strlen(tmp->string) + 1);
	strcpy(result, tmp->string);
	return result;
}

void delete_node(Node* node, List list) {
	if(node == NULL) {
		return;
	}

	Node* tmp = list.head;
	Node* parent = list.head;
	while(tmp->next != NULL) {
		parent = tmp;
		tmp = tmp->next;
		if(tmp == node) {
			parent->next = tmp->next;
			free(tmp);
		}
	}
}