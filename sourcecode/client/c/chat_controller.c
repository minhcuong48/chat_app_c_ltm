#include <chat_controller.h>
#include <gtk/gtk.h>
#include <protocol.h>

static void on_send_button_clicked(GtkButton* button, gpointer data) {
    ChatController* this = (ChatController*) data;
    char* friend_username = this->friend_username;
    ChatView* chat_view = this->chat_view;
    GtkEntry* entry = (GtkEntry*)(chat_view->entry);
    gchar* text = gtk_entry_get_text(entry);
    // need to type some text before send
    if(text == NULL || strcmp(text, "") == 0) {
        return;
    }
    GtkTextView* text_view = (GtkTextView*)(chat_view->text_view);
    chat_view_append_line(chat_view, "Me", text);
    char* code_mess = get_code_mess(friend_username, text);
    send_message_to_server(code_mess);
    gtk_entry_set_text(entry, "");
    free(code_mess);
}

static gboolean on_close_button_clicked(GtkButton* button, GdkEvent *event, gpointer data) {
    ChatController* this = (ChatController*) data;
    ChatView* chat_view = this->chat_view;
    GtkWidget* window = chat_view->window;
    gtk_widget_hide(window);
    return TRUE;
}

static void chat_controller_setup_events(ChatController* this) {
    g_signal_connect(G_OBJECT(this->chat_view->send_button), "clicked", G_CALLBACK(on_send_button_clicked), this);
    g_signal_connect(G_OBJECT(this->chat_view->window), "delete_event", G_CALLBACK(on_close_button_clicked), this);
}

ChatController* chat_controller_new(char* friend_username) {
    ChatController* this = (ChatController*) malloc(sizeof(ChatController));
    this->chat_view = chat_view_new();
    this->friend_username = friend_username;
    gtk_window_set_title(this->chat_view->window, friend_username);
    chat_controller_setup_events(this);
    return this;
}

void chat_controller_start(ChatController* this) {
    chat_view_show(this->chat_view);
}

