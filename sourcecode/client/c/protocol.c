#include <protocol.h>
#include <stdlib.h>
#include <constant.h>

char* get_code_login(char* username, char* password) {
	char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "LGIN|");
	strcat(code, username);
	strcat(code, "|");
	strcat(code, password);
	return code;
}

char* get_code_signup(char* username, char* password) {
	char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "SGNU|");
	strcat(code, username);
	strcat(code, "|");
	strcat(code, password);
	return code;
}

char* get_code_mess(char* receiver, char* message) {
	char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "MESS|");
	strcat(code, receiver);
	strcat(code, "|");
	strcat(code, message);
	return code;
}

char* get_code_friend_request(char* receiver) {
	char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "FREQ|");
	strcat(code, receiver);
	return code;
}

char* get_code_accept_request(char* requester) {
	char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "ACCR|");
	strcat(code, requester);
	return code;
}

char* get_code_decline_request(char* requester) {
	char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "DECR|");
	strcat(code, requester);
	return code;
}

char* get_code_delete_friend(char* username) {
	char* code = (char*)malloc(MAX_STRING_LENGTH_PROTOCOL);
	strcpy(code, "DELF|");
	strcat(code, username);
	return code;
}