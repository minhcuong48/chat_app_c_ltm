#include <chat_manager.h>
#include <string.h>

ChatManager* chat_manager_new() {
    ChatManager* this = (ChatManager*) malloc(sizeof(ChatManager));
    this->count = 0;
    return this;
}

ChatController* chat_manager_add(ChatManager* chat_manager, char* friend_username) {
    char* _friend_username = strdup(friend_username);
    ChatController* chat_controller = chat_controller_new(_friend_username);
    int count = chat_manager->count;
    chat_manager->chat_controllers[count] = chat_controller;
    chat_manager->count++;
    chat_controller_start(chat_controller);
    return chat_controller;
}

ChatController* chat_manager_get(ChatManager* chat_manager, char* friend_username) {
    int i = 0;
    for(i = 0; i < chat_manager->count; i++) {
        char* _friend_username = chat_manager->chat_controllers[i]->friend_username;
        if(strcmp(_friend_username, friend_username) == 0) {
            return chat_manager->chat_controllers[i];
        }
    }
    return NULL;
}
void chat_manager_pass_message(ChatManager* chat_manager, char* friend_username, char* message) {
    ChatController* chat_controller = chat_manager_get(chat_manager, friend_username);
    if(chat_controller == NULL) {
        chat_controller = chat_manager_add(chat_manager, friend_username);
    }
    gtk_widget_show_all(GTK_WIDGET(chat_controller->chat_view->window));
    chat_view_append_line(chat_controller->chat_view, friend_username, message);
}