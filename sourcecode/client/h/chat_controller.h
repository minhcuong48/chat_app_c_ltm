#ifndef _CHAT_CONTROLLER_H_
#define _CHAT_CONTROLLER_H_

#include <chat_view.h>

typedef struct _ChatController {
    char* friend_username;
    ChatView* chat_view;
} ChatController;

ChatController* chat_controller_new(char* friend_username);
void chat_controller_start(ChatController* this);

#endif