#ifndef _THREAD_H_
#define _THREAD_H_

#include <pthread.h>
#include <main_controller.h>
#include <chat_manager.h>

typedef struct _Thread {
    pthread_t thread_id;
    int server;
    MainController* main_controller;
    ChatManager* chat_manager;
} Thread;

Thread* thread_new(int server, MainController* main_controller, ChatManager* chat_manager);

#endif