#ifndef _CHAT_VIEW_H_
#define _CHAT_VIEW_H_

#include <gtk/gtk.h>

typedef struct _ChatView {
    GtkWidget *window, *main_alignment, *main_container;
    GtkWidget *scrolled_window, *text_view;
    GtkWidget *send_button, *entry, *bottom_container;
} ChatView;

ChatView* chat_view_new();
void chat_view_show(ChatView *view);
void chat_view_append_line(ChatView* this, char* sender, char* message);
void chat_view_scroll_to_bottom(ChatView* this);

#endif