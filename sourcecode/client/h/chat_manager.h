#ifndef _CHAT_MANAGER_H_
#define _CHAT_MANAGER_H_

#include <chat_controller.h>
#include <constant.h>

typedef struct _ChatManager {
    ChatController* chat_controllers[MAX_CONNECTION];
    int count;
} ChatManager;

ChatManager* chat_manager_new();
ChatController* chat_manager_add(ChatManager* chat_manager, char* friend_username);
ChatController* chat_manager_get(ChatManager* chat_manager, char* friend_username);
void chat_manager_pass_message(ChatManager* chat_manager, char* friend_username, char* message);

#endif