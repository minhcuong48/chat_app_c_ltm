#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

char* get_code_login(char* username, char* password);
char* get_code_signup(char* username, char* password);
char* get_code_mess(char* receiver, char* message);
char* get_code_friend_request(char* receiver);
char* get_code_accept_request(char* requester);
char* get_code_decline_request(char* requester);
char* get_code_delete_friend(char* username);

#endif