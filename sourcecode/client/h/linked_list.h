#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

#define LINKED_LIST_STRING_LENGTH_MAX 300

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _Node {
	char string[300];
	struct _Node *next;
} Node;

typedef struct _List {
	int count;
	int current;
	Node* head;
} List;

Node* create_head();
List create_list();
Node* create_node(char* string);
Node* add_node(List* list, Node* node);
Node* get_last_node(List* list);
Node* get_next_node(List* list);
int count_nodes(List list);
void print_list(List list);
char* get_node_string(int index, List list);
void delete_node(Node* node, List list);

#endif