#ifndef _MAIN_CONTROLLER_H_
#define _MAIN_CONTROLLER_H_

#include <main_view.h>
#include <chat_manager.h>

typedef struct _MainController {
    MainView* main_view;
    ChatManager* chat_manager;
} MainController;

MainController* main_controller_new(ChatManager* chat_manager);
void main_controller_start(MainController* this);

#endif