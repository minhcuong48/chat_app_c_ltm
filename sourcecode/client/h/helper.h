#ifndef _HELPER_H_
#define _HELPER_H_

#include <arpa/inet.h>
#include <linked_list.h>
#include <constant.h>
#include <gtk/gtk.h>

typedef struct _Bundle {
    void* data[100];
    int size;
} Bundle;

extern int server;

int send_message_to_server(char* message);
int send_message(int server, char* message);
int isEndWithSeparator(const char* message);
char* get_header(char* message);
int count_message_tokens(char* message);
List extract_message_to_list(char* message);
char* sub_string(char* source, int from, int to);
char* current_date_time();
void append_int(char* string, int integer);
void log_info(char* string);
int connect_server(char* server_ip, int port);
void quick_message(gchar* message);

// for bundle
Bundle* bundle_new();
void bundle_add(Bundle* b, void* item);

#endif