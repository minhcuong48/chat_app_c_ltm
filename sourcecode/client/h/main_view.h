#ifndef _MAIN_VIEW_H_
#define _MAIN_VIEW_H_

#include <gtk/gtk.h>

typedef struct _MainView {
    GtkWidget *window, *main_alignment, *main_container;
    GtkWidget *username_label, *username_entry, *password_label, *password_entry;
    GtkWidget *buttons_container, *login_button, *signup_button;

    GtkWidget *add_friend_container, *friend_username_entry, *add_friend_button;
    
    GtkWidget *invitation_tree_view;
    GtkTreeStore *invitation_tree_store;

    GtkWidget *accept_button, *decline_button;
    GtkWidget *invitation_buttons_container;

    GtkWidget *friend_tree_view;
    GtkTreeStore *friend_tree_store;

    GtkWidget *delete_friend_button;

    char* current_username;
} MainView;

enum {
    FRIEND_COLUMN,
    STATUS_COLUMN,
    FRIEND_NUM_COLUMNS
};

enum {
    INVITATION_COLUMN,
    INVITATION_NUM_COLUMNS
};

MainView* main_view_new();
void main_view_show(MainView* view);
void main_view_show_main(MainView *view);
void main_view_append_friend(MainView* main_view, char* username, char* status);
void main_view_append_invitation(MainView* main_view, char* invitation);
void main_view_set_friend_status(MainView* main_view, char* username, char* status);
void main_view_delete_friend(MainView* main_view, char* username);

#endif